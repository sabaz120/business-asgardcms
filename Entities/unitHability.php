<?php

namespace Modules\Business\Entities;

use Illuminate\Database\Eloquent\Model;

class unitHability extends Model
{

    protected $table = 'business__unithabilities';
    protected $fillable = [
      'description',
      'unit_id'
    ];

    public function unit()
    {
        return $this->BelongsTo('Modules\Business\Entities\Unit', 'unit_id');
    }
}
