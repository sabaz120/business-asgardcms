<?php

namespace Modules\Business\Entities;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{

    protected $table = 'business__units';
    protected $fillable = [
      "name",
      "description",
      "status",
      "summary",
      "required_profile",
      "type_education",
      "minimal_experience",
      "code",
      "parent_id"
    ];

    public function businesses()
    {
      // return $this->belongsToMany(Business::class, 'business__businessunits')->withPivot('id','business_id')->withTimestamps()->using(BusinessUnit::class);
        return $this->hasMany('Modules\Business\Entities\BusinessUnit', 'unit_id');
    }
    public function habilities()
    {
        return $this->hasMany('Modules\Business\Entities\unitHability', 'unit_id');
    }

    public function unitFunctions()
    {
        return $this->hasMany('Modules\Business\Entities\BusinessUnitfunctions', 'unit_id');
    }
}
