<?php

namespace Modules\Business\Entities;

use Illuminate\Database\Eloquent\Model;

class BusinessUnitfunctions extends Model
{
    protected $table = 'business_unitfunctions';
    protected $fillable = [
      'description',
      "unit_id"
    ];

    public function unit()
    {
        return $this->BelongsTo('Modules\Business\Entities\Unit', 'unit_id');
    }
}
