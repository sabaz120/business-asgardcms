<?php

namespace Modules\Business\Entities;

use Illuminate\Database\Eloquent\Model;

class BusinessUnit extends Model
{

    protected $table = 'business__businessunits';
    protected $fillable = [
      "business_id",
      "unit_id",
    ];

    public function unit()
    {
        return $this->BelongsTo('Modules\Business\Entities\Unit', 'unit_id');
    }
    public function business()
    {
        return $this->BelongsTo('Modules\Business\Entities\Business', 'business_id');
    }
}
