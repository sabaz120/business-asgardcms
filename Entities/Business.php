<?php

namespace Modules\Business\Entities;

use Illuminate\Database\Eloquent\Model;

class Business extends Model
{

    protected $table = 'business__businesses';
    protected $fillable = [
      "name",
      "description",
      "rif",
      "status",
      "parent_id",
      "address",
      'parish_id'
    ];

    public function children()
    {
        return $this->hasMany('Modules\Business\Entities\Business', 'parent_id');
    }
    public function parentBusiness()
    {
        return $this->BelongsTo('Modules\Business\Entities\Business', 'parent_id');
    }
    public function businesses()
    {
        return $this->hasMany('Modules\Business\Entities\BusinessUnit', 'business_id');
    }
    public function parish(){
      return $this->belongsTo('Modules\Locations\Entities\Parish','parish_id');
    }
}
