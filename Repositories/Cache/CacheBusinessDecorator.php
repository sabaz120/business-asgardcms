<?php

namespace Modules\Business\Repositories\Cache;

use Modules\Business\Repositories\BusinessRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheBusinessDecorator extends BaseCacheDecorator implements BusinessRepository
{
    public function __construct(BusinessRepository $business)
    {
        parent::__construct();
        $this->entityName = 'business.businesses';
        $this->repository = $business;
    }
}
