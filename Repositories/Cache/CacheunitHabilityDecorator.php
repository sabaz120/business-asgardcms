<?php

namespace Modules\Business\Repositories\Cache;

use Modules\Business\Repositories\unitHabilityRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheunitHabilityDecorator extends BaseCacheDecorator implements unitHabilityRepository
{
    public function __construct(unitHabilityRepository $unithability)
    {
        parent::__construct();
        $this->entityName = 'business.unithabilities';
        $this->repository = $unithability;
    }
}
