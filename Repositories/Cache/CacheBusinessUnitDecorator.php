<?php

namespace Modules\Business\Repositories\Cache;

use Modules\Business\Repositories\BusinessUnitRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheBusinessUnitDecorator extends BaseCacheDecorator implements BusinessUnitRepository
{
    public function __construct(BusinessUnitRepository $businessunit)
    {
        parent::__construct();
        $this->entityName = 'business.businessunits';
        $this->repository = $businessunit;
    }
}
