<?php

namespace Modules\Business\Repositories\Cache;

use Modules\Business\Repositories\UnitRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheUnitDecorator extends BaseCacheDecorator implements UnitRepository
{
    public function __construct(UnitRepository $unit)
    {
        parent::__construct();
        $this->entityName = 'business.units';
        $this->repository = $unit;
    }
}
