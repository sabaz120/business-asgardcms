<?php

namespace Modules\Business\Repositories;

use Modules\Core\Repositories\BaseRepository;

interface UnitRepository extends BaseRepository
{
}
