<?php

namespace Modules\Business\Repositories\Eloquent;

use Modules\Business\Repositories\unitHabilityRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentunitHabilityRepository extends EloquentBaseRepository implements unitHabilityRepository
{
}
