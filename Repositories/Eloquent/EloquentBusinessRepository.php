<?php

namespace Modules\Business\Repositories\Eloquent;

use Modules\Business\Repositories\BusinessRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentBusinessRepository extends EloquentBaseRepository implements BusinessRepository
{

  public function getParentBusiness(){
    return $this->model->where('parent_id',0)->get();
  }
}
