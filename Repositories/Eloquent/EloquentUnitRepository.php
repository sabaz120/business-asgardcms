<?php

namespace Modules\Business\Repositories\Eloquent;

use Modules\Business\Repositories\UnitRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentUnitRepository extends EloquentBaseRepository implements UnitRepository
{
}
