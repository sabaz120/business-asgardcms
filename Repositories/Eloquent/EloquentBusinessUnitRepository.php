<?php

namespace Modules\Business\Repositories\Eloquent;

use Modules\Business\Repositories\BusinessUnitRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentBusinessUnitRepository extends EloquentBaseRepository implements BusinessUnitRepository
{
}
