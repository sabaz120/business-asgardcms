<?php

namespace Modules\Business\Repositories;

use Modules\Core\Repositories\BaseRepository;

interface BusinessUnitRepository extends BaseRepository
{
}
