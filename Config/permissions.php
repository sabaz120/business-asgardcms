<?php

return [
    'business.businesses' => [
        'index' => 'business::businesses.list resource',
        'create' => 'business::businesses.create resource',
        'edit' => 'business::businesses.edit resource',
        'destroy' => 'business::businesses.destroy resource',
    ],
    'business.units' => [
        'index' => 'business::units.list resource',
        'create' => 'business::units.create resource',
        'edit' => 'business::units.edit resource',
        'destroy' => 'business::units.destroy resource',
    ],
    'business.businessunits' => [
        'index' => 'business::businessunits.list resource',
        'create' => 'business::businessunits.create resource',
        'edit' => 'business::businessunits.edit resource',
        'destroy' => 'business::businessunits.destroy resource',
    ],
    'business.unithabilities' => [
        'index' => 'business::unithabilities.list resource',
        'create' => 'business::unithabilities.create resource',
        'edit' => 'business::unithabilities.edit resource',
        'destroy' => 'business::unithabilities.destroy resource',
    ],
// append






];
