<?php

namespace Modules\Business\Events\Handlers;

use Maatwebsite\Sidebar\Group;
use Maatwebsite\Sidebar\Item;
use Maatwebsite\Sidebar\Menu;
use Modules\Core\Events\BuildingSidebar;
use Modules\User\Contracts\Authentication;

class RegisterBusinessSidebar implements \Maatwebsite\Sidebar\SidebarExtender
{
    /**
     * @var Authentication
     */
    protected $auth;

    /**
     * @param Authentication $auth
     *
     * @internal param Guard $guard
     */
    public function __construct(Authentication $auth)
    {
        $this->auth = $auth;
    }

    public function handle(BuildingSidebar $sidebar)
    {
        $sidebar->add($this->extendWith($sidebar->getMenu()));
    }

    /**
     * @param Menu $menu
     * @return Menu
     */
    public function extendWith(Menu $menu)
    {
        $menu->group(trans('core::sidebar.content'), function (Group $group) {
            $group->item(trans('business::businesses.title.businesses'), function (Item $item) {
                $item->icon('fa fa-copy');
                $item->weight(10);
                $item->authorize(
                     $this->auth->hasAccess('business.businesses.index')
                );
                $item->item(trans('business::businesses.title.businesses'), function (Item $item) {
                    $item->icon('fa fa-copy');
                    $item->weight(0);
                    $item->append('admin.business.business.create');
                    $item->route('admin.business.business.index');
                    $item->authorize(
                        $this->auth->hasAccess('business.businesses.index')
                    );
                });
                $item->item(trans('business::units.title.units'), function (Item $item) {
                    $item->icon('fa fa-copy');
                    $item->weight(0);
                    $item->append('admin.business.unit.create');
                    $item->route('admin.business.unit.index');
                    $item->authorize(
                        $this->auth->hasAccess('business.units.index')
                    );
                });
                // $item->item(trans('business::businessunits.title.businessunits'), function (Item $item) {
                //     $item->icon('fa fa-copy');
                //     $item->weight(0);
                //     $item->append('admin.business.businessunit.create');
                //     $item->route('admin.business.businessunit.index');
                //     $item->authorize(
                //         $this->auth->hasAccess('business.businessunits.index')
                //     );
                // });
                // $item->item(trans('business::unithabilities.title.unithabilities'), function (Item $item) {
                //     $item->icon('fa fa-copy');
                //     $item->weight(0);
                //     $item->append('admin.business.unithability.create');
                //     $item->route('admin.business.unithability.index');
                //     $item->authorize(
                //         $this->auth->hasAccess('business.unithabilities.index')
                //     );
                // });
// append






            });
        });

        return $menu;
    }
}
