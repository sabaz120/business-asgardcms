<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBusinessUnitfunctionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('business_unitfunctions', function (Blueprint $table) {
            $table->increments('id');
            $table->text('description');
            $table->integer('unit_id')->unsigned();
            $table->foreign('unit_id')->references('id')->on('business__units');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('business_unitfunctions');
    }
}
