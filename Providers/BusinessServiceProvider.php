<?php

namespace Modules\Business\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Core\Traits\CanPublishConfiguration;
use Modules\Core\Events\BuildingSidebar;
use Modules\Core\Events\LoadingBackendTranslations;
use Modules\Business\Events\Handlers\RegisterBusinessSidebar;

class BusinessServiceProvider extends ServiceProvider
{
    use CanPublishConfiguration;
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerBindings();
        $this->app['events']->listen(BuildingSidebar::class, RegisterBusinessSidebar::class);

        $this->app['events']->listen(LoadingBackendTranslations::class, function (LoadingBackendTranslations $event) {
            $event->load('businesses', array_dot(trans('business::businesses')));
            $event->load('units', array_dot(trans('business::units')));
            $event->load('businessunits', array_dot(trans('business::businessunits')));
            $event->load('unithabilities', array_dot(trans('business::unithabilities')));
            // append translations






        });
    }

    public function boot()
    {
        $this->publishConfig('business', 'permissions');

        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }

    private function registerBindings()
    {
        $this->app->bind(
            'Modules\Business\Repositories\BusinessRepository',
            function () {
                $repository = new \Modules\Business\Repositories\Eloquent\EloquentBusinessRepository(new \Modules\Business\Entities\Business());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Business\Repositories\Cache\CacheBusinessDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Business\Repositories\UnitRepository',
            function () {
                $repository = new \Modules\Business\Repositories\Eloquent\EloquentUnitRepository(new \Modules\Business\Entities\Unit());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Business\Repositories\Cache\CacheUnitDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Business\Repositories\BusinessUnitRepository',
            function () {
                $repository = new \Modules\Business\Repositories\Eloquent\EloquentBusinessUnitRepository(new \Modules\Business\Entities\BusinessUnit());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Business\Repositories\Cache\CacheBusinessUnitDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Business\Repositories\unitHabilityRepository',
            function () {
                $repository = new \Modules\Business\Repositories\Eloquent\EloquentunitHabilityRepository(new \Modules\Business\Entities\unitHability());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Business\Repositories\Cache\CacheunitHabilityDecorator($repository);
            }
        );
// add bindings






    }
}
