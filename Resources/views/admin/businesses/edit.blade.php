@extends('layouts.master')

@section('content-header')
    <h1>
        {{ trans('business::businesses.title.edit business') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li><a href="{{ route('admin.business.business.index') }}">{{ trans('business::businesses.title.businesses') }}</a></li>
        <li class="active">{{ trans('business::businesses.title.edit business') }}</li>
    </ol>
@stop

@section('content')
    {!! Form::open(['route' => ['admin.business.business.update', $business->id], 'method' => 'put']) !!}
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                @include('partials.form-tab-headers')
                <div class="tab-content">
                    @include('business::admin.businesses.partials.edit-fields')
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary btn-flat">{{ trans('core::core.button.update') }}</button>
                        <a class="btn btn-danger pull-right btn-flat" href="{{ route('admin.business.business.index')}}"><i class="fa fa-times"></i> {{ trans('core::core.button.cancel') }}</a>
                    </div>
                </div>
            </div> {{-- end nav-tabs-custom --}}
        </div>
    </div>
    {!! Form::close() !!}
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>b</code></dt>
        <dd>{{ trans('core::core.back to index') }}</dd>
    </dl>
@stop

@push('js-stack')
    <script type="text/javascript">

        $( document ).ready(function() {
            $(document).keypressAction({
                actions: [
                    { key: 'b', route: "<?= route('admin.business.business.index') ?>" }
                ]
            });
        });
    </script>
    <script>
        $( document ).ready(function() {
            $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
                checkboxClass: 'icheckbox_flat-blue',
                radioClass: 'iradio_flat-blue'
            });
            loadStates();
            var parish_id="{{$business->parish_id}}";
            if(parish_id!=0){
              var municipality_id="{{$business->parish ? $business->parish->municipality_id : 0}}";
              var state_id="{{$business->parish ? $business->parish->municipality->state_id : 0}}";
              loadMunicipalities(state_id);
              loadParishes(municipality_id);
            }
        });
        function loadStates(){
          //Load cities
          $.ajax({
            url:"{{url('/')}}"+'/states',
            type:'GET',
            headers:{'X-CSRF-TOKEN': "{{csrf_token()}}"},
            dataType:"json",
            data:{},
            success:function(result){
              var cities=result;
              var html="";
              var parish_id="{{$business->parish_id}}";
              var state_id="{{$business->parish ? $business->parish->municipality->state_id : 0}}";
              for(var i=0;i<cities.length;i++)
                html+='<option value="'+cities[i].id+'">'+cities[i].name+'</option>';
              $('#state_id').html(html);
              if(parish_id!=0)
                $('#state_id option[value='+state_id+']').attr('selected','selected');
            },
            error:function(error){
              console.log(error);
            }
          });//ajax
        }

        function loadMunicipalities(state_id=null){
          if(state_id==null){
            state_id=$('#state_id').val()
          }
          $.ajax({
            url:"{{url('/')}}"+'/municipalities',
            type:'GET',
            headers:{'X-CSRF-TOKEN': "{{csrf_token()}}"},
            dataType:"json",
            data:{state_id:state_id},
            success:function(result){
              var municipalities=result;
              var html="";
              for(var i=0;i<municipalities.length;i++)
                html+='<option value="'+municipalities[i].id+'">'+municipalities[i].name+'</option>';
              $('#municipality_id').html(html);
              var municipality_id="{{$business->parish ? $business->parish->municipality_id : 0}}";
              $('#municipality_id option[value='+municipality_id+']').attr('selected','selected');
              $('#divMunicipalities').show();
              $('#divParishes').hide();
              $('#parish_id').val(null);
            },
            error:function(error){
              console.log(error);
            }
          });//ajax
        }//loadMunicipalities()

        function loadParishes(municipality_id=null){
          if(municipality_id==null){
            municipality_id=$('#municipality_id').val();
          }
          $.ajax({
            url:"{{url('/')}}"+'/parishes',
            type:'GET',
            headers:{'X-CSRF-TOKEN': "{{csrf_token()}}"},
            dataType:"json",
            data:{municipality_id:municipality_id},
            success:function(result){
              var parishes=result;
              var html="";
              var parish_id="{{$business->parish_id}}";
              for(var i=0;i<parishes.length;i++){
                if(i==0)
                html+='<option value="'+parishes[i].id+'" selected>'+parishes[i].name+'</option>';
                else
                html+='<option value="'+parishes[i].id+'">'+parishes[i].name+'</option>';
              }
              $('#parish_id').html(html);
              $('#divParishes').show();
              $('#parish_id option[value='+parish_id+']').attr('selected','selected');
            },
            error:function(error){
              console.log(error);
            }
          });//ajax
        }//loadParishes()
    </script>
@endpush
