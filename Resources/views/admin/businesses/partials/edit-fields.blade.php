<div class="box-body row">
  <div class="form-group col-md-4">
    <label>RIF </label>
    <input type="text" class="form-control" name="rif" value="{{$business->rif}}" placeholder="Rif de la empresa">
  </div>
  <div class="form-group col-md-4">
    <label>Nombre </label>
    <input type="text" class="form-control" name="name" value="{{$business->name}}" placeholder="Nombre de la empresa">
  </div>
  <div class="form-group col-md-4">
    <label>Dirección </label>
    <input type="text" class="form-control" name="address" value="{{$business->address}}" placeholder="Dirección de la empresa">
  </div>
  <div class="form-group col-md-4">
    <label>Empresa padre </label>
    <select class="form-control" name="parent_id">
      <option value="0">Seleccione una empresa</option>
      @foreach($parentBusiness as $PBusiness)
      @if($PBusiness->id!=$business->id)
        @if($PBusiness->id==$business->parent_id)
          <option value="{{$PBusiness->id}}" selected>{{$PBusiness->name}}</option>
        @else
          <option value="{{$PBusiness->id}}">{{$PBusiness->name}}</option>
        @endif
      @endif
      @endforeach
    </select>
  </div>
  <div class="form-group col-md-4">
    <label>Mostrar en el menu</label>
    <select class="form-control"  id="status" name="status" >
      <option value="0" @if($business->status ==0 ) selected @endif>No</option>
      <option value="1" @if($business->status ==1 ) selected @endif>Si</option>
    </select>
  </div>
  <div class="form-group col-md-4">
    <label>Estado</label>
    <select class="form-control" onchange="loadMunicipalities()" id="state_id">

    </select>
  </div>
  <div class="form-group col-md-4" onchange="loadParishes()" style="display:none;" id="divMunicipalities">
    <label>Municipio</label>
    <select class="form-control"  id="municipality_id">

    </select>
  </div>
  <div class="form-group col-md-4" style="display:none;" id="divParishes">
    <label>Parroquia</label>
    <select class="form-control" required id="parish_id" name="parish_id">

    </select>
  </div>
  <div class="form-group col-md-12">
    <?php $old = $business->description ?>
    @editor('description', "Descripción", old("description",$old))
  </div>
</div>
