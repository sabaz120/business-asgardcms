<div class="box-body row">
  <div class="form-group col-md-4">
    <label>RIF </label>
    <input type="text" class="form-control" required name="rif" value="{{old('rif')}}" placeholder="Rif de la empresa">
  </div>
  <div class="form-group col-md-4">
    <label>Nombre </label>
    <input type="text" class="form-control" required name="name" value="{{old('name')}}" placeholder="Nombre de la empresa">
  </div>
  <div class="form-group col-md-4">
    <label>Dirección </label>
    <input type="text" class="form-control" required name="address" value="{{old('address')}}" placeholder="Dirección de la empresa">
  </div>
  <div class="form-group col-md-4">
    <label>Empresa padre </label>
    <select class="form-control" required name="parent_id">
      <option value="0">Seleccione una empresa</option>
      @foreach($parentBusiness as $business)
      <option value="{{$business->id}}">{{$business->name}}</option>
      @endforeach
    </select>
  </div>
  <div class="form-group col-md-4">
    <label>Mostrar en el menu</label>
    <select class="form-control"  id="status" name="status">
      <option value="0">No</option>
      <option value="1">Si</option>
    </select>
  </div>
  <div class="form-group col-md-4">
    <label>Estado</label>
    <select class="form-control" onchange="loadMunicipalities()" id="state_id">
    </select>
  </div>
  <div class="form-group col-md-4" onchange="loadParishes()" style="display:none;" id="divMunicipalities">
    <label>Municipio</label>
    <select class="form-control"  id="municipality_id">

    </select>
  </div>
  <div class="form-group col-md-12" style="display:none;" id="divParishes">
    <label>Parroquia</label>
    <select class="form-control" required id="parish_id" name="parish_id">

    </select>
  </div>
  <div class="form-group col-md-12">
    @editor('description', "Descripción", old("description"))
  </div>
</div>
