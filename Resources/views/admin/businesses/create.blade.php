@extends('layouts.master')

@section('content-header')
    <h1>
        {{ trans('business::businesses.title.create business') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li><a href="{{ route('admin.business.business.index') }}">{{ trans('business::businesses.title.businesses') }}</a></li>
        <li class="active">{{ trans('business::businesses.title.create business') }}</li>
    </ol>
@stop

@section('content')
    {!! Form::open(['route' => ['admin.business.business.store'], 'method' => 'post']) !!}
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                @include('partials.form-tab-headers')
                <div class="tab-content">
                    @include('business::admin.businesses.partials.create-fields')
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary btn-flat">{{ trans('core::core.button.create') }}</button>
                        <a class="btn btn-danger pull-right btn-flat" href="{{ route('admin.business.business.index')}}"><i class="fa fa-times"></i> {{ trans('core::core.button.cancel') }}</a>
                    </div>
                </div>
            </div> {{-- end nav-tabs-custom --}}
        </div>
    </div>
    {!! Form::close() !!}
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>b</code></dt>
        <dd>{{ trans('core::core.back to index') }}</dd>
    </dl>
@stop

@push('js-stack')
    <script type="text/javascript">
        $( document ).ready(function() {
            $(document).keypressAction({
                actions: [
                    { key: 'b', route: "<?= route('admin.business.business.index') ?>" }
                ]
            });
        });
    </script>
    <script>
        $( document ).ready(function() {
            $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
                checkboxClass: 'icheckbox_flat-blue',
                radioClass: 'iradio_flat-blue'
            });
            //Load cities
            $.ajax({
              url:"{{url('/')}}"+'/states',
              type:'GET',
              headers:{'X-CSRF-TOKEN': "{{csrf_token()}}"},
              dataType:"json",
              data:{},
              success:function(result){
                var cities=result;
                var html="";
                for(var i=0;i<cities.length;i++)
                  html+='<option value="'+cities[i].id+'">'+cities[i].name+'</option>';
                console.log(html);
                $('#state_id').html(html);
              },
              error:function(error){
                console.log(error);
              }
            });//ajax
        });

        function loadMunicipalities(){
          $.ajax({
            url:"{{url('/')}}"+'/municipalities',
            type:'GET',
            headers:{'X-CSRF-TOKEN': "{{csrf_token()}}"},
            dataType:"json",
            data:{state_id:$('#state_id').val()},
            success:function(result){
              var municipalities=result;
              var html="";
              for(var i=0;i<municipalities.length;i++)
                html+='<option value="'+municipalities[i].id+'">'+municipalities[i].name+'</option>';
              $('#municipality_id').html(html);
              $('#divMunicipalities').show();
              $('#divParishes').hide();
              $('#parish_id').val(null);
            },
            error:function(error){
              console.log(error);
            }
          });//ajax
        }//loadMunicipalities()

        function loadParishes(){
          $.ajax({
            url:"{{url('/')}}"+'/parishes',
            type:'GET',
            headers:{'X-CSRF-TOKEN': "{{csrf_token()}}"},
            dataType:"json",
            data:{municipality_id:$('#municipality_id').val()},
            success:function(result){
              var parishes=result;
              var html="";
              for(var i=0;i<parishes.length;i++){
                if(i==0)
                html+='<option value="'+parishes[i].id+'" selected>'+parishes[i].name+'</option>';
                else
                html+='<option value="'+parishes[i].id+'">'+parishes[i].name+'</option>';
              }
              $('#parish_id').html(html);
              $('#divParishes').show();
            },
            error:function(error){
              console.log(error);
            }
          });//ajax
        }//loadParishes()






    </script>
@endpush
