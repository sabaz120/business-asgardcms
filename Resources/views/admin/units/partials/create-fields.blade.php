<div class="box-body">
  <div class="form-row">
    <div class="form-group col-md-3">
      <label for="">Nombre</label>
      <input type="text" class="form-control" required name="name" value="">
    </div>
    <div class="form-group col-md-3">
      <label for="">Código</label>
      <input type="text" class="form-control" required name="code" value="">
    </div>
    <div class="form-group col-md-3">
      <label for="">Experiencia mínima(En años)</label>
      <input type="number" class="form-control"  required name="minimal_experience" min="0">
    </div>
    <div class="form-group col-md-3">
      <label>Cargo superior</label>
      <select class="form-control" name="parent_id">
        <option value="0">Seleccione un cargo</option>
        @foreach($units as $unit)
        <option value="{{$unit->id}}">{{$unit->name}}</option>
        @endforeach
      </select>
    </div>
  </div>

  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="">Nivel de instrucción</label>
      <select class="form-control" name="type_education" required>
        <option value="0">Seleccione su nivel de instrucción</option>
        <option value="Primaria" Primaria</option>
        <option value="Secundaria"  >Secundaria</option>
        <option value="Técnico medio" >Técnico medio</option>
        <option value="Programa Nacional de Aprendizaje(PNA)">Programa Nacional de Aprendizaje(PNA)</option>
        <option value="Técnico Superior Universitario (TSU)" >Técnico Superior Universitario (TSU)</option>
        <option value="Universitaria (pregrado)" >Universitaria (pregrado)</option>
        <option value="Postgrado-Especialización" >Postgrado - Especialización</option>
        <option value="Postgrado-Maestría" >Postgrado - Maestría</option>
        <option value="Postgrado-Doctorado" >Postgrado - Doctorado</option>
      </select>
    </div>
    <div class="form-group col-md-6">
      <label>Mostrar en el menu</label>
      <select class="form-control"  id="status" name="status" >
        <option value="0" >No</option>
        <option value="1" >Si</option>
      </select>
    </div>

  </div>

  <div class="form-row">
    <div class="form-group col-md-12">
      <label for="">Resumen</label>
      <textarea class="form-control" name="summary" required rows="8" cols="80" style="resize: none;"></textarea>
    </div>
  </div>

  <div class="form-row">
    <div class="form-group col-md-6">
      @editor('required_profile', "Perfil requerido", old("required_profile"))
      <!-- <input type="text" class="form-control" required name="required_profile" value=""> -->
    </div>

    <div class="form-group col-md-6">
      @editor('description', "Descripción", old("description"))
    </div>
    <div class="col-md-12">
      <br>
      <label for="">Empresas</label>
      @if(count($businesses)>0)

      <div class="checkbox" style="list-style: none;padding-left: 5px;">
        @foreach ($businesses as $business)
          @if($business->parent_id==0)
              <label st>
                <input type="checkbox" class="flat-blue jsInherit" name="businesses[]" value="{{$business->id}}" > {{$business->name}}
              </label>
              <!-- CHILDREN BUSINESS -->
              @if(count($business->children)>0)
                @php
                $children=$business->children
                @endphp
                @if(count($children)>0)


                  @foreach ($children as $x => $child)


                    <label>

                      <input type="checkbox" class="flat-blue jsInherit" name="businesses[]" value="{{$child->id}}"> {{$child->name}}

                    </label>


                  @endforeach


                @endif
              @endif
              <!-- CHILDREN BUSINESS -->
          @endif
        @endforeach
      </div>
      @endif
    </div>
    <input type="hidden" name="habilities" id="habilities" value="">
    <input type="hidden" name="unitFunction" id="unitFunction" value="">
  </div>


  <div class="form-row text-center col-lg-6">
    <label for="">Funciones del cargo</label>
  <br>
    <div class="form-group col-md-6 col-md-offset-3">
      <label>Por favor ingresa las funciones del cargo</label>
        <div class="input-group">
          <input type="text" class="form-control" id="inputFunction">
          <span type="button" class="input-group-addon btn btn-success" onclick="addUnitFunctions()">Agregar</span>
        </div>

      </div>

      <div class="col-md-12 text-center table-responsive">
        <table id="tableFunction" class="table table-bordered table-shape">
          <thead>
            <tr>
              <td>#</td>
              <td>Función</td>
              <td>Acción</td>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td colspan="3">No se han ingresado habilidades</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>


<div class="form-row text-center col-lg-6">
  <label for="">Habilidades y Destrezas</label>
<br>
  <div class="form-group col-md-6 col-md-offset-3">
    <label>Descripción de la habilidad</label>
      <div class="input-group">
        <input type="text" class="form-control" id="inputHability">
        <span type="button" class="input-group-addon btn btn-success" onclick="addHability()">Agregar</span>
      </div>

    </div>

    <div class="col-md-12 text-center table-responsive">
      <table id="tableHabilities" class="table table-bordered table-shape">
        <thead>
          <tr>
            <td>#</td>
            <td>Habilidad</td>
            <td>Acción</td>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td colspan="3">No se han ingresado habilidades</td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>

</div>
