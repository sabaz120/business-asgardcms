
<div class="box-body">
  <div class="form-group col-md-3">
    <label for="">Nombre</label>
    <input type="text" class="form-control" required name="name" value="{{$unit->name}}">
  </div>
  <div class="form-group col-md-3">
    <label for="">Código</label>
    <input type="text" class="form-control" required name="code" value="{{$unit->code}}">
  </div>
  <div class="form-group col-md-3">
    <label for="">Experiencia mínima</label>
    <input type="number" class="form-control"  value="{{$unit->minimal_experience}}" required name="minimal_experience" min="0">
  </div>
  <div class="form-group col-md-3">
    <label>Cargo superior</label>
    <select class="form-control" name="parent_id">
      <option value="0">Seleccione un cargo</option>
      @foreach($parentUnits as $pUnit)
      @if($pUnit->id!=$unit->id)
      @if($pUnit->id==$unit->parent_id)
      <option value="{{$pUnit->id}}" selected>{{$pUnit->name}}</option>
      @else
      <option value="{{$pUnit->id}}">{{$pUnit->name}}</option>
      @endif
      @endif
      @endforeach
    </select>
  </div>
  <div class="form-group col-md-6">
    <label for="">Nivel de instrucción</label>
    <select class="form-control" name="type_education" required>
      <option value="0">Seleccione su nivel de instrucción</option>
      <option value="Primaria" @if($unit->type_education == 'Primaria') selected @endif>Primaria</option>
      <option value="Secundaria"  @if($unit->type_education == 'Secundaria') selected @endif >Secundaria</option>
      <option value="Técnico medio" @if($unit->type_education == 'Técnico medio') selected @endif >Técnico medio</option>
      <option value="Programa Nacional de Aprendizaje(PNA)" @if($unit->type_education == 'Programa Nacional de Aprendizaje(PNA)') selected @endif>Programa Nacional de Aprendizaje(PNA)</option>
      <option value="Técnico Superior Universitario (TSU)" @if($unit->type_education == 'Técnico Superior Universitario (TSU)') selected @endif>Técnico Superior Universitario (TSU)</option>
      <option value="Universitaria (pregrado)" @if($unit->type_education == 'Universitaria (pregrado)') selected @endif  >Universitaria (pregrado)</option>
      <option value="Postgrado-Especialización" @if($unit->type_education == 'Postgrado-Especialización') selected @endif >Postgrado - Especialización</option>
      <option value="Postgrado-Maestría"> @if($unit->type_education == 'Postgrado-Maestría') selected @endif >Postgrado - Maestría</option>
      <option value="Postgrado-Doctorado" @if($unit->type_education == 'Postgrado-Doctorado') selected @endif >Postgrado - Doctorado</option>
    </select>
  </div>

  <div class="form-group col-md-6">
    <label>Mostrar en el menu</label>
    <select class="form-control"  id="status" name="status" >
      <option value="0" >No</option>
      <option value="1">Si</option>
    </select>
  </div>
  <div class="form-group col-md-12">
    <label for="">Resumen</label>
    <textarea class="form-control" name="summary" required rows="8" cols="80" style="resize: none;">{{$unit->summary}}</textarea>
  </div>
  <div class="form-group col-md-12">
    <?php $old = $unit->required_profile ?>
    @editor('required_profile', "Perfil requerido", old("required_profile",$old))
  </div>
  <div class="form-group col-md-12">
    <?php $old = $unit->description ?>
    @editor('description', "Descripción", old("description",$old))
  </div>
  <div class="col-md-12">
    <br><br>
    <label for="">Empresas</label>
    @if(count($businesses)>0)
        @php

            if(isset($unit->businesses) && count($unit->businesses)>0){

                $oldCat = array();

                foreach ($unit->businesses as $cat){

                    array_push($oldCat,$cat->business_id);

                }

            }

        @endphp
    <div class="checkbox" style="list-style: none;padding-left: 5px;">

      @foreach ($businesses as $business)


      @if($business->parent_id==0)


        <label>

          <input type="checkbox" class="flat-blue jsInherit" name="businesses[]"

          value="{{$business->id}}" @isset($oldCat) @if(in_array($business->id, $oldCat)) checked="checked" @endif @endisset> {{$business->name}}

        </label>
        <!-- CHILDREN BUSINESS -->
        @if(count($business->children)>0)
          @php
          $children=$business->children
          @endphp
          @if(count($children)>0)


            @foreach ($children as $x => $child)


              <label>

                <input type="checkbox" class="flat-blue jsInherit" name="businesses[]" value="{{$child->id}}" @isset($oldCat) @if(in_array($child->id, $oldCat)) checked="checked" @endif @endisset> {{$child->name}}

              </label>


            @endforeach


          @endif
        @endif
        <!-- CHILDREN BUSINESS -->

      @endif

      @endforeach

    </ul>
    @endif
  </div>


  <div class="form-row text-center col-lg-6">
    <label for="">Funciones del cargo</label>
  <br>
    <div class="form-group col-md-6 col-md-offset-3">
      <label>Por favor ingresa las funciones del cargo</label>
        <div class="input-group">
          <input type="text" class="form-control" id="inputFunction">
          <span type="button" class="input-group-addon btn btn-success" onclick="addUnitFunctions()">Agregar</span>
        </div>

      </div>

      <div class="col-md-12 text-center table-responsive">
        <table id="tableFunction" class="table table-bordered table-shape">
          <thead>
            <tr>
              <td>#</td>
              <td>Función</td>
              <td>Acción</td>
            </tr>
          </thead>
          <tbody>
            <tr>
              @if(count($unit->unitFunctions)==null)
              <td colspan="3">No se han ingresado funciones</td>
              @else
              @foreach($unit->unitFunctions as $item)
              <script type="text/javascript">
              unitFunction.push("{{$item->description}}");
              </script>
              @endforeach
              @endif
            </tr>
          </tbody>
        </table>
      </div>
    </div>

  <div class="col-md-6 form-group ">
    <input type="hidden" name="habilities" id="habilities" value="">
    <input type="hidden" name="unitFunction" id="unitFunction" value="">
    <label for="" class="text-center col-md-offset-6">Habilidades y Destrezas</label>
    <div class="col-md-12 text-center mt-2">
      <div class="input-group">
        <input type="text" class="form-control" id="inputHability">
        <span type="button" class="input-group-addon btn btn-success" onclick="addHability()">Agregar</span>
      </div>
    </div>

    <div class="col-md-12 text-center table-responsive mt-2">
      <table id="tableHabilities" class="table table-bordered table-shape">
        <thead>
          <tr>
            <td> <strong>#</strong> </td>
            <td> <strong>Habilidad</strong> </td>
            <td> <strong>Acción</strong> </td>
          </tr>
        </thead>
        <tbody>
          <tr>
            @if(count($unit->habilities)==0)
            <td colspan="3">No se han ingresado habilidades</td>
            @else
            @foreach($unit->habilities as $hability)
            <script type="text/javascript">
            habilities.push("{{$hability->description}}");
            </script>
            @endforeach
            @endif
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>
