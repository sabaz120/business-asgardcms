@extends('layouts.master')

@section('content-header')
    <h1>
        {{ trans('business::units.title.create unit') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li><a href="{{ route('admin.business.unit.index') }}">{{ trans('business::units.title.units') }}</a></li>
        <li class="active">{{ trans('business::units.title.create unit') }}</li>
    </ol>
@stop

@section('content')
    {!! Form::open(['route' => ['admin.business.unit.store'], 'method' => 'post']) !!}
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                @include('partials.form-tab-headers')
                <div class="tab-content">
                    @include('business::admin.units.partials.create-fields')
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary btn-flat">{{ trans('core::core.button.create') }}</button>
                        <a class="btn btn-danger pull-right btn-flat" href="{{ route('admin.business.unit.index')}}"><i class="fa fa-times"></i> {{ trans('core::core.button.cancel') }}</a>
                    </div>
                </div>
            </div> {{-- end nav-tabs-custom --}}
        </div>
    </div>
    {!! Form::close() !!}
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>b</code></dt>
        <dd>{{ trans('core::core.back to index') }}</dd>
    </dl>
@stop

@push('js-stack')
    <script type="text/javascript">
    var habilities=[];
    var unitFunction=[];
        $( document ).ready(function() {
            $(document).keypressAction({
                actions: [
                    { key: 'b', route: "<?= route('admin.business.unit.index') ?>" }
                ]
            });
        });
    </script>
    <script>
        $( document ).ready(function() {
            $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
                checkboxClass: 'icheckbox_flat-blue',
                radioClass: 'iradio_flat-blue'
            });
        });

        function deleteHability(index){
          habilities.splice(index,1);
          loadHabilitiesTable();
        }
        function deleteUnitFunctions(index){
          unitFunction.splice(index,1);
          loadFunctionTable();
        }
        function loadHabilitiesTable(){
          var html="";
          for(var i=0;i<habilities.length;i++){
            html+="<tr>";
            html+="<td>";
            html+=i+1;
            html+="</td>";
            html+="<td>";
            html+=habilities[i];
            html+="</td>";
            html+="<td>";
            html+='<button type="button" class="btn btn-danger" onclick="deleteHability('+i+')" name="button"><i class="fa fa-trash"></i></button>';
            html+="</td>";
            html+="</tr>";
          }//for
          if(habilities.length==0){
            html+="<tr>";
            html+="<td colspan='3'>No se han ingresado habilidades";
            html+="</td>";
            html+="</tr>";
          }
          $('#tableHabilities tbody').html(html);
          $('#habilities').val(JSON.stringify(habilities));
        }

        function loadFunctionTable(){
          var html2="";
          for(var i=0;i<unitFunction.length;i++){
            html2+="<tr>";
            html2+="<td>";
            html2+=i+1;
            html2+="</td>";
            html2+="<td>";
            html2+=unitFunction[i];
            html2+="</td>";
            html2+="<td>";
            html2+='<button type="button" class="btn btn-danger" onclick="deleteUnitFunctions('+i+')" name="button"><i class="fa fa-trash"></i></button>';
            html2+="</td>";
            html2+="</tr>";
          }//for
          if(unitFunction.length==0){
            html2+="<tr>";
            html2+="<td colspan='3'>No se han ingresado funciones";
            html2+="</td>";
            html2+="</tr>";
          }
          $('#tableFunction tbody').html(html2);
          $('#unitFunction').val(JSON.stringify(unitFunction));
        }
        function addHability(){
          var html="";
          var descHability=$('#inputHability').val();
          if(descHability=="")
            alert("Debe escribir una habilidad u destreza.");
          habilities.push(descHability);
          $('#inputHability').val("");
          loadHabilitiesTable();
        }//addHability()

        loadHabilitiesTable();

        // FUNCIONES DEL CARGO
        function addUnitFunctions(){
          var html2="";
          var descFunction=$('#inputFunction').val();
          console.log(descFunction);
          if(descFunction=="")
            alert("Debe escribir una funcion del cargo.");
          unitFunction.push(descFunction);
          $('#inputFunction').val("");
          loadFunctionTable();
        }//addHability()
    </script>
@endpush
