<?php

return [
    'list resource' => 'Lista de cargos',
    'create resource' => 'Crear cargos',
    'edit resource' => 'Editar cargos',
    'destroy resource' => 'Borrar cargos',
    'title' => [
        'units' => 'Cargo',
        'create unit' => 'Crear un cargo',
        'edit unit' => 'Editar un cargo',
    ],
    'button' => [
        'create unit' => 'Crear un cargo',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
