<?php

return [
    'list resource' => 'List businessunits',
    'create resource' => 'Create businessunits',
    'edit resource' => 'Edit businessunits',
    'destroy resource' => 'Destroy businessunits',
    'title' => [
        'businessunits' => 'BusinessUnit',
        'create businessunit' => 'Create a businessunit',
        'edit businessunit' => 'Edit a businessunit',
    ],
    'button' => [
        'create businessunit' => 'Create a businessunit',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
