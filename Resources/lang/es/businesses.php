<?php

return [
    'list resource' => 'Lista de empresas',
    'create resource' => 'Crear empresas',
    'edit resource' => 'Editar empresas',
    'destroy resource' => 'Borrar empresas',
    'title' => [
        'businesses' => 'Empresa',
        'create business' => 'Crear una empresa',
        'edit business' => 'Editar una empresa',
    ],
    'button' => [
        'create business' => 'Crear una empresa',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
