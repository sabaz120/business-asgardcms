<?php

return [
    'list resource' => 'List unithabilities',
    'create resource' => 'Create unithabilities',
    'edit resource' => 'Edit unithabilities',
    'destroy resource' => 'Destroy unithabilities',
    'title' => [
        'unithabilities' => 'unitHability',
        'create unithability' => 'Create a unithability',
        'edit unithability' => 'Edit a unithability',
    ],
    'button' => [
        'create unithability' => 'Create a unithability',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
