<?php

use Illuminate\Routing\Router;
$locale = LaravelLocalization::setLocale() ?: App::getLocale();

$router->get('businesses', ['as' => 'business.businesses', 'uses' => 'BusinessController@businesses']);
$router->get('unitBusiness/{business_id}', ['as' => 'business.unit.business', 'uses' => 'BusinessController@unitsBusiness']);
$router->get('randomHabilities/{unit_id}', ['as' => 'unit.random.habilities', 'uses' => 'UnitHabilitiesController@randomHabilities']);
