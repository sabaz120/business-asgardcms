<?php

use Illuminate\Routing\Router;
/** @var Router $router */

$router->group(['prefix' =>'/business'], function (Router $router) {
    $router->bind('business', function ($id) {
        return app('Modules\Business\Repositories\BusinessRepository')->find($id);
    });
    $router->get('businesses', [
        'as' => 'admin.business.business.index',
        'uses' => 'BusinessController@index',
        'middleware' => 'can:business.businesses.index'
    ]);
    $router->get('businesses/create', [
        'as' => 'admin.business.business.create',
        'uses' => 'BusinessController@create',
        'middleware' => 'can:business.businesses.create'
    ]);
    $router->post('businesses', [
        'as' => 'admin.business.business.store',
        'uses' => 'BusinessController@store',
        'middleware' => 'can:business.businesses.create'
    ]);
    $router->get('businesses/{business}/edit', [
        'as' => 'admin.business.business.edit',
        'uses' => 'BusinessController@edit',
        'middleware' => 'can:business.businesses.edit'
    ]);
    $router->put('businesses/{business}', [
        'as' => 'admin.business.business.update',
        'uses' => 'BusinessController@update',
        'middleware' => 'can:business.businesses.edit'
    ]);
    $router->delete('businesses/{business}', [
        'as' => 'admin.business.business.destroy',
        'uses' => 'BusinessController@destroy',
        'middleware' => 'can:business.businesses.destroy'
    ]);
    $router->bind('unit', function ($id) {
        return app('Modules\Business\Repositories\UnitRepository')->find($id);
    });
    $router->get('units', [
        'as' => 'admin.business.unit.index',
        'uses' => 'UnitController@index',
        'middleware' => 'can:business.units.index'
    ]);
    $router->get('units/create', [
        'as' => 'admin.business.unit.create',
        'uses' => 'UnitController@create',
        'middleware' => 'can:business.units.create'
    ]);
    $router->post('units', [
        'as' => 'admin.business.unit.store',
        'uses' => 'UnitController@store',
        'middleware' => 'can:business.units.create'
    ]);
    $router->get('units/{unit}/edit', [
        'as' => 'admin.business.unit.edit',
        'uses' => 'UnitController@edit',
        'middleware' => 'can:business.units.edit'
    ]);
    $router->put('units/{unit}', [
        'as' => 'admin.business.unit.update',
        'uses' => 'UnitController@update',
        'middleware' => 'can:business.units.edit'
    ]);
    $router->delete('units/{unit}', [
        'as' => 'admin.business.unit.destroy',
        'uses' => 'UnitController@destroy',
        'middleware' => 'can:business.units.destroy'
    ]);
    $router->bind('businessunit', function ($id) {
        return app('Modules\Business\Repositories\BusinessUnitRepository')->find($id);
    });
    $router->get('businessunits', [
        'as' => 'admin.business.businessunit.index',
        'uses' => 'BusinessUnitController@index',
        'middleware' => 'can:business.businessunits.index'
    ]);
    $router->get('businessunits/create', [
        'as' => 'admin.business.businessunit.create',
        'uses' => 'BusinessUnitController@create',
        'middleware' => 'can:business.businessunits.create'
    ]);
    $router->post('businessunits', [
        'as' => 'admin.business.businessunit.store',
        'uses' => 'BusinessUnitController@store',
        'middleware' => 'can:business.businessunits.create'
    ]);
    $router->get('businessunits/{businessunit}/edit', [
        'as' => 'admin.business.businessunit.edit',
        'uses' => 'BusinessUnitController@edit',
        'middleware' => 'can:business.businessunits.edit'
    ]);
    $router->put('businessunits/{businessunit}', [
        'as' => 'admin.business.businessunit.update',
        'uses' => 'BusinessUnitController@update',
        'middleware' => 'can:business.businessunits.edit'
    ]);
    $router->delete('businessunits/{businessunit}', [
        'as' => 'admin.business.businessunit.destroy',
        'uses' => 'BusinessUnitController@destroy',
        'middleware' => 'can:business.businessunits.destroy'
    ]);
    $router->bind('unithability', function ($id) {
        return app('Modules\Business\Repositories\unitHabilityRepository')->find($id);
    });
    $router->get('unithabilities', [
        'as' => 'admin.business.unithability.index',
        'uses' => 'unitHabilityController@index',
        'middleware' => 'can:business.unithabilities.index'
    ]);
    $router->get('unithabilities/create', [
        'as' => 'admin.business.unithability.create',
        'uses' => 'unitHabilityController@create',
        'middleware' => 'can:business.unithabilities.create'
    ]);
    $router->post('unithabilities', [
        'as' => 'admin.business.unithability.store',
        'uses' => 'unitHabilityController@store',
        'middleware' => 'can:business.unithabilities.create'
    ]);
    $router->get('unithabilities/{unithability}/edit', [
        'as' => 'admin.business.unithability.edit',
        'uses' => 'unitHabilityController@edit',
        'middleware' => 'can:business.unithabilities.edit'
    ]);
    $router->put('unithabilities/{unithability}', [
        'as' => 'admin.business.unithability.update',
        'uses' => 'unitHabilityController@update',
        'middleware' => 'can:business.unithabilities.edit'
    ]);
    $router->delete('unithabilities/{unithability}', [
        'as' => 'admin.business.unithability.destroy',
        'uses' => 'unitHabilityController@destroy',
        'middleware' => 'can:business.unithabilities.destroy'
    ]);
// append






});
