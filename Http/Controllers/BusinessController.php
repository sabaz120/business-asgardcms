<?php

namespace Modules\Business\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Core\Http\Controllers\BasePublicController;
use Modules\Business\Entities\Business;
use Modules\Business\Entities\BusinessUnit;

class BusinessController extends BasePublicController
{
  public function __construct()
  {
    parent::__construct();
  }

  public function businesses(Request $request){
    $business=Business::query();
    if(isset($request->filters)){
      $filters=$request->filters;
      $filters=json_decode($filters);
      if(isset($filters->status)){
        $business->where('status',$filters->status);
      }
    }
    $business=$business->get();
    return response()->json($business,200);
  }//businesses
  public function unitsBusiness($business_id,Request $request){
    $units=BusinessUnit::query();
    if(isset($request->filters)){
      $filters=$request->filters;
      $filters=json_decode($filters);
      if(isset($filters->status)){
        $units->whereHas('unit',function($query) use($filters){
          $query->where('status',$filters->status);
        });
      }
    }
    $units=$units->where('business_id',$business_id)->with('business','unit')->get();
    // $units=BusinessUnit::where('business_id',$business_id)->with('business','unit')->get();
    return response()->json($units,200);
  }//storeAspirant()

}
