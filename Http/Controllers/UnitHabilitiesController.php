<?php

namespace Modules\Business\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Core\Http\Controllers\BasePublicController;
use Modules\Business\Entities\Business;
use Modules\Business\Entities\unitHability;

class UnitHabilitiesController extends BasePublicController
{
  public function __construct()
  {
    parent::__construct();
  }

  public function randomHabilities($unit_id){
    $habilities=unitHability::with('unit')->where('unit_id',$unit_id)->inRandomOrder()->get();
    $randomHabilities=unitHability::with('unit')->whereNotIn('unit_id',[$unit_id])->inRandomOrder()->limit(4)->get();
    $merge=$habilities->merge($randomHabilities);
    $merge=$merge->shuffle();
    $merge=$merge->all();
    return response()->json($merge,200);
  }//randomHabilities($unit_id)

}
