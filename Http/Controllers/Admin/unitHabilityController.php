<?php

namespace Modules\Business\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Business\Entities\unitHability;
use Modules\Business\Http\Requests\CreateunitHabilityRequest;
use Modules\Business\Http\Requests\UpdateunitHabilityRequest;
use Modules\Business\Repositories\unitHabilityRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class unitHabilityController extends AdminBaseController
{
    /**
     * @var unitHabilityRepository
     */
    private $unithability;

    public function __construct(unitHabilityRepository $unithability)
    {
        parent::__construct();

        $this->unithability = $unithability;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //$unithabilities = $this->unithability->all();

        return view('business::admin.unithabilities.index', compact(''));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('business::admin.unithabilities.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateunitHabilityRequest $request
     * @return Response
     */
    public function store(CreateunitHabilityRequest $request)
    {
        $this->unithability->create($request->all());

        return redirect()->route('admin.business.unithability.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('business::unithabilities.title.unithabilities')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  unitHability $unithability
     * @return Response
     */
    public function edit(unitHability $unithability)
    {
        return view('business::admin.unithabilities.edit', compact('unithability'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  unitHability $unithability
     * @param  UpdateunitHabilityRequest $request
     * @return Response
     */
    public function update(unitHability $unithability, UpdateunitHabilityRequest $request)
    {
        $this->unithability->update($unithability, $request->all());

        return redirect()->route('admin.business.unithability.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('business::unithabilities.title.unithabilities')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  unitHability $unithability
     * @return Response
     */
    public function destroy(unitHability $unithability)
    {
        $this->unithability->destroy($unithability);

        return redirect()->route('admin.business.unithability.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('business::unithabilities.title.unithabilities')]));
    }
}
