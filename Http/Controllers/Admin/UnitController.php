<?php

namespace Modules\Business\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Business\Entities\Unit;
use Modules\Business\Entities\Business;
use Modules\Business\Entities\BusinessUnit;
use Modules\Business\Entities\unitHability;
use Modules\Business\Entities\BusinessUnitfunctions;
use Modules\Preselection\Entities\Aspirants;
use Modules\Business\Http\Requests\CreateUnitRequest;
use Modules\Business\Http\Requests\UpdateUnitRequest;
use Modules\Business\Repositories\UnitRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class UnitController extends AdminBaseController
{
    /**
     * @var UnitRepository
     */
    private $unit;

    public function __construct(UnitRepository $unit)
    {
        parent::__construct();

        $this->unit = $unit;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $units = $this->unit->all();

        return view('business::admin.units.index', compact('units'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $units = $this->unit->all();
        $businesses=Business::all();
        return view('business::admin.units.create',compact('units',"businesses"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateUnitRequest $request
     * @return Response
     */
    public function store(CreateUnitRequest $request)
    {
        $unit=$this->unit->create($request->all());
        foreach(json_decode($request->habilities) as $hability){
          unitHability::create([
            'unit_id'=>$unit->id,
            'description'=>$hability
          ]);
        }

        foreach(json_decode($request->unitFunction) as $functions){
          BusinessUnitfunctions::create([
            'unit_id'=>$unit->id,
            'description'=>$functions
          ]);
        }

        if (isset($request->businesses) && $request->businesses) {
          BusinessUnit::where('unit_id',$unit->id)->delete();
          foreach($request->businesses as $business){
            BusinessUnit::create([
              "business_id"=>$business,
              "unit_id"=>$unit->id
            ]);
          }
        }else
          BusinessUnit::where('unit_id',$unit->id)->delete();
        return redirect()->route('admin.business.unit.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('business::units.title.units')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Unit $unit
     * @return Response
     */
    public function edit(Unit $unit)
    {
        $parentUnits = $this->unit->all();
        $businesses=Business::all();
        $habilities=[];
        return view('business::admin.units.edit', array('unit'=>$unit,'parentUnits'=>$parentUnits,'businesses'=>$businesses));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Unit $unit
     * @param  UpdateUnitRequest $request
     * @return Response
     */
    public function update(Unit $unit, UpdateUnitRequest $request)
    {
        $this->unit->update($unit, $request->all());
        if (isset($request->businesses) && $request->businesses) {
          BusinessUnit::where('unit_id',$unit->id)->delete();
          foreach($request->businesses as $business){
            BusinessUnit::create([
              "business_id"=>$business,
              "unit_id"=>$unit->id
            ]);
          }
        }else
          BusinessUnit::where('unit_id',$unit->id)->delete();
        //Create news habilities
        $habilities=unitHability::where('unit_id',$unit->id)->get();
        foreach(json_decode($request->habilities) as $hability){
          $dbHability=unitHability::where('unit_id',$unit->id)->where('description',$hability)->first();
          if(!$dbHability){
            unitHability::create([
              'unit_id'=>$unit->id,
              'description'=>$hability
            ]);
          }
        }
        //Crear y borrar funciones de cargo
        BusinessUnitfunctions::where('unit_id',$unit->id)->delete();
        foreach(json_decode($request->unitFunction) as $functions){
          BusinessUnitfunctions::create([
            'unit_id'=>$unit->id,
            'description'=>$functions
          ]);
        }
        //FIN

        //delete habilities
        $newHabilities=json_decode($request->habilities);
        foreach($habilities as $dbHability){
          $b=0;
          for($i=0;$i<count($newHabilities);$i++){
            if($dbHability->description==$newHabilities[$i]){
              $b=1;
              break;
            }
          }//second for
          if($b==0)
            unitHability::where('id',$dbHability->id)->delete();
        }//for
        return redirect()->route('admin.business.unit.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('business::units.title.units')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Unit $unit
     * @return Response
     */
    public function destroy(Unit $unit)
    {
      $aspirants=Aspirants::where('unity_id',$unit->id)->get();
      if(count($aspirants)>0){
        return redirect()->back()->withError("No puedes borrar el cargo, existen aspirantes asociados a este cargo.");
      }
        $this->unit->destroy($unit);

        return redirect()->route('admin.business.unit.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('business::units.title.units')]));
    }
}
