<?php

namespace Modules\Business\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Business\Entities\Business;
use Modules\Preselection\Entities\Aspirants;
use Modules\Business\Http\Requests\CreateBusinessRequest;
use Modules\Business\Http\Requests\UpdateBusinessRequest;
use Modules\Business\Repositories\BusinessRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class BusinessController extends AdminBaseController
{
    /**
     * @var BusinessRepository
     */
    private $business;

    public function __construct(BusinessRepository $business)
    {
        parent::__construct();

        $this->business = $business;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $businesses = $this->business->all();
        return view('business::admin.businesses.index', compact('businesses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
      $parentBusiness=$this->business->getParentBusiness();
        return view('business::admin.businesses.create',compact('parentBusiness'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateBusinessRequest $request
     * @return Response
     */
    public function store(CreateBusinessRequest $request)
    {
        $this->business->create($request->all());

        return redirect()->route('admin.business.business.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('business::businesses.title.businesses')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Business $business
     * @return Response
     */
    public function edit(Business $business)
    {
      $parentBusiness=$this->business->getParentBusiness();
        return view('business::admin.businesses.edit', compact('business','parentBusiness'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Business $business
     * @param  UpdateBusinessRequest $request
     * @return Response
     */
    public function update(Business $business, UpdateBusinessRequest $request)
    {
        $this->business->update($business, $request->all());

        return redirect()->route('admin.business.business.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('business::businesses.title.businesses')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Business $business
     * @return Response
     */
    public function destroy(Business $business)
    {
        $aspirants=Aspirants::where('business_id',$business->id)->get();
        if(count($aspirants)>0){
          return redirect()->back()->withError("No puedes borrar la empresa, existen aspirantes asociados a esta empresa.");
        }
        $this->business->destroy($business);

        return redirect()->route('admin.business.business.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('business::businesses.title.businesses')]));
    }
}
