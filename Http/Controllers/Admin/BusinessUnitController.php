<?php

namespace Modules\Business\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Business\Entities\BusinessUnit;
use Modules\Business\Http\Requests\CreateBusinessUnitRequest;
use Modules\Business\Http\Requests\UpdateBusinessUnitRequest;
use Modules\Business\Repositories\BusinessUnitRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class BusinessUnitController extends AdminBaseController
{
    /**
     * @var BusinessUnitRepository
     */
    private $businessunit;

    public function __construct(BusinessUnitRepository $businessunit)
    {
        parent::__construct();

        $this->businessunit = $businessunit;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //$businessunits = $this->businessunit->all();

        return view('business::admin.businessunits.index', compact(''));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('business::admin.businessunits.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateBusinessUnitRequest $request
     * @return Response
     */
    public function store(CreateBusinessUnitRequest $request)
    {
        $this->businessunit->create($request->all());

        return redirect()->route('admin.business.businessunit.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('business::businessunits.title.businessunits')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  BusinessUnit $businessunit
     * @return Response
     */
    public function edit(BusinessUnit $businessunit)
    {
        return view('business::admin.businessunits.edit', compact('businessunit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  BusinessUnit $businessunit
     * @param  UpdateBusinessUnitRequest $request
     * @return Response
     */
    public function update(BusinessUnit $businessunit, UpdateBusinessUnitRequest $request)
    {
        $this->businessunit->update($businessunit, $request->all());

        return redirect()->route('admin.business.businessunit.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('business::businessunits.title.businessunits')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  BusinessUnit $businessunit
     * @return Response
     */
    public function destroy(BusinessUnit $businessunit)
    {
        $this->businessunit->destroy($businessunit);

        return redirect()->route('admin.business.businessunit.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('business::businessunits.title.businessunits')]));
    }
}
