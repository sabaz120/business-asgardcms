<?php

namespace Modules\Business\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class CreateBusinessRequest extends BaseFormRequest
{
    public function rules()
    {
        return [
          'name'=>'required|min:2',
          'address'=>'required|min:4',
          'description'=>'required|min:5',
          'parish_id'=>'required|exists:locations__parishes,id'
        ];
    }

    public function translationRules()
    {
        return [];
    }

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [];
    }

    public function translationMessages()
    {
        return [];
    }
}
